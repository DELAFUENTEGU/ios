//
//  API.swift
//  PracticaIOS_DeCas
//
//  Created by Alejandro Castro on 6/12/17.
//  Copyright © 2017 DeCas. All rights reserved.
//

import Foundation
import UIKit
import LorAPI

final class API {
    
    //also charge the image
    
    let urlApi = JSONDeCas()
    
    //let URL_USERS = "https://www.jasonbase.com/things/y8Bm.json"
    //let URL_MENUDATA = "https://www.jasonbase.com/things/84oD.json"
    //let URL_WEAPONDATA = "https://www.jasonbase.com/things/Jokb.json"
    
    //var URL_USERS :String
    //var URL_MENUDATA :String
    //var URL_WEAPONDATA :String
    
    var URL_USERS = ""
    var URL_MENUDATA = ""
    var URL_WEAPONDATA = ""
    
    func initURLs(){
        
        URL_USERS = urlApi.userURL()
        URL_MENUDATA = urlApi.menuURL()
        URL_WEAPONDATA = urlApi.weaponURL()
        
        print(URL_USERS)
        print("https://www.jasonbase.com/things/y8Bm.json")
    }
    
    func getUsers(completionHandler: @escaping ([GameUsers])-> Void){
        guard let url = URL(string: urlApi.userURL())else{return}
        let dataTask = URLSession.shared.dataTask(with: url, completionHandler: { data, response, error -> Void in
            let jsonObj = self.urlApi.getDictionary(data: data!)
            
            var game_user: [GameUsers] = []
            
            if let usersArray = jsonObj.value(forKey: "users") as? NSArray {
                
                usersArray.forEach { user in
                    
                    if let userDict = user as? NSDictionary {
                        if let name = (userDict.value(forKey: "name") as? String),
                            let password = (userDict.value(forKey: "password") as? String),
                            let image = (userDict.value(forKey: "image") as? String),
                            let games_wins = (userDict.value(forKey: "wins") as? String),
                            let games_losts = (userDict.value(forKey: "lost") as? String),
                            let total_kills = (userDict.value(forKey: "kills") as? String),
                            let fav_weapon = (userDict.value(forKey: "favweapon") as? String){
                            
                            game_user.append(GameUsers(
                            
                                name: name,
                                password: password,
                                image: image,
                                games_wins: games_wins,
                                games_losts: games_losts,
                                total_kills: total_kills,
                                fav_weapon: fav_weapon
                            ))

                        }
                    }
                    
                }
                
                completionHandler(game_user)
            }
        })
        
        dataTask.resume()
    }
    

    
    func getMenuData(completionHandler: @escaping ([MenuData])-> Void){
        guard let url = URL(string: urlApi.menuURL())else{return}
        let dataTask = URLSession.shared.dataTask(with: url, completionHandler: { data, response, error -> Void in
            let jsonObj = self.urlApi.getDictionary(data: data!)
            //guard let data = data, let jsonObj = try? JSONSerialization.jsonObject(with: data, options: .allowFragments) as? NSDictionary else {return}
           // print(jsonObj?.value(forKey: "menudata") ?? "")
            
            var menu_data: [MenuData] = []
            
            if let menuDataArray = jsonObj.value(forKey: "menudata") as? NSArray {
                
                menuDataArray.forEach { data_menu in
                    
                    if let menuDataDict = data_menu as? NSDictionary {
                        if let data = (menuDataDict.value(forKey: "data") as? String),
                            let image = (menuDataDict.value(forKey: "image") as? String) {
                            
                            menu_data.append(MenuData(
                                text_data: data,
                                image: image))
                            
                        }
                    }
                    
                }
                
                completionHandler(menu_data)
            }
        })
        
        dataTask.resume()
    }
    
    func getWeaponData(completionHandler: @escaping ([WeaponData])-> Void){
        guard let url = URL(string: urlApi.weaponURL())else{return}
        let dataTask = URLSession.shared.dataTask(with: url, completionHandler: { data, response, error -> Void in
            let jsonObj = self.urlApi.getDictionary(data: data!)
            //guard let data = data, let jsonObj = try? JSONSerialization.jsonObject(with: data, options: .allowFragments) as? NSDictionary else {
             //   return
            //}
            
            
           // print(jsonObj?.value(forKey: "weapondata") ?? "")
            
            var weapon_data: [WeaponData] = []
            
            if let weaponDataArray = jsonObj.value(forKey: "weapondata") as? NSArray {
                
                weaponDataArray.forEach { data_weap in
                    
                    if let menuDataDict = data_weap as? NSDictionary {
                        
                        if let name = (menuDataDict.value(forKey: "name") as? String),
                            let ammo = (menuDataDict.value(forKey: "ammo") as? String),
                            let damage = (menuDataDict.value(forKey: "damage") as? String),
                            let firerate = (menuDataDict.value(forKey: "firerate") as? String),
                            let image = (menuDataDict.value(forKey: "image") as? String) {
                            
                            weapon_data.append(WeaponData(
                                name: name,
                                ammo: ammo,
                                damage: damage,
                                firerate: firerate,
                                image: image))
                            
                        }
                    }
                    
                }
                
                completionHandler(weapon_data)
            }
        })
        
        dataTask.resume()
        
    }
    
    
    
    //used if the user is offline, I guess
    
    func downloadImage(imageUrl: String) -> UIImage? {
        guard let url = URL(string: imageUrl), let data = try? Data(contentsOf: url) else {
            return nil
        }
        return UIImage(data: data)
    }
    
    
    
    
    
}
