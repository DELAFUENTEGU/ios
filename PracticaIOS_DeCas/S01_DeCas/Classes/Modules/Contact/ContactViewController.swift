//
//  ContactViewController.swift
//  PracticaIOS_DeCas
//
//  Created by Alejandro Castro on 17/12/17.
//  Copyright © 2017 DeCas. All rights reserved.
//

import UIKit
import MessageUI

class ContactViewController: UIViewController, MFMailComposeViewControllerDelegate {

    @IBOutlet var sendButton: UIButton!
    @IBOutlet var msgContent: UITextView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func sendEmail() {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients(["delafuentegu@esat-alumni.com"])
            mail.setMessageBody(msgContent.text, isHTML: true)
            
            present(mail, animated: true)
            let alert = UIAlertView(title: "Notice", message: "Message sent!", delegate: nil, cancelButtonTitle: "Ok")
            alert.show()
        } else {
            let alert = UIAlertView(title: "Error", message: "Error sending the message.", delegate: nil, cancelButtonTitle: "Ok")
            alert.show()
        }
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
    
    @IBAction func pressed() {
        sendEmail()
    }
    
}
