//
//  ItemCell.swift
//  PracticaIOS_DeCas
//
//  Created by Alejandro Castro on 15/12/17.
//  Copyright © 2017 DeCas. All rights reserved.
//

import UIKit

class ItemCell: UITableViewCell {

    @IBOutlet var ImageView: UIImageView!
    
    @IBOutlet var TextCell: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
