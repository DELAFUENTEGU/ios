//
//  MenuViewController.swift
//  PracticaIOS_DeCas
//
//  Created by pvesat on 5/12/17.
//  Copyright © 2017 DeCas. All rights reserved.
//

import UIKit
import KFSwiftImageLoader


 class MenuViewController: UIViewController {

    
    @IBOutlet var tableView: UITableView!
    
    // Array data of the menu
    var menu_data : [MenuData] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Title of the view
        self.title = "Menu"
        // Configure the menu table
        configureTable()
        // Configure the menu data
        configureMenuData()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Configures the table and add a cell based in the dates of the menu json
    private func configureTable(){
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.tableView.separatorStyle = .singleLine
        self.tableView.rowHeight = 161 //UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 161
        
        let bandCellNib = UINib(nibName: "ItemCell", bundle: nil)
        
        // Add the new cell to the table
        self.tableView.register(bandCellNib, forCellReuseIdentifier: "ItemCell")
    }
    
    // Read the menu data json from the API and store the data in the menu data array
    private func configureMenuData(){
        // Read the data
        API().getMenuData { mdata in
            // Store the data
            self.menu_data = mdata
            // Reload using main thread
            DispatchQueue.main.async(execute: {
                self.tableView.reloadData()
            })
        }
    }
}

// Extension
extension MenuViewController: UITableViewDelegate{
    // Menu cell selected
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:     // First menu row selected
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            // Load weapon data view
            let controller = storyboard.instantiateViewController(withIdentifier: "WeaponViewController")
            self.navigationController?.pushViewController(controller, animated: true)
            break
            
        case 1:     // Second menu row selected
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            // Load user info view
            let controller = storyboard.instantiateViewController(withIdentifier: "UserInfoViewController")
            self.navigationController?.pushViewController(controller, animated: true)
            break
            
        case 2:     // Third menu row selected
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            // Load contact view
            let controller = storyboard.instantiateViewController(withIdentifier: "ContactViewController")
            self.navigationController?.pushViewController(controller, animated: true)
            break
        default:
            print("can not open new view")
        }
    }
}

// Extension
extension MenuViewController : UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    // return the number of elements in menu data
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.menu_data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Save menu data element
        let data_info = self.menu_data[indexPath.row]
        // Configure the menu table view for the selected menu data element
        return self.configureMenuTableViewCell(menu_data: data_info)
    }
    
    func configureMenuTableViewCell(menu_data: MenuData) -> UITableViewCell {
        guard let cell = self.tableView.dequeueReusableCell(withIdentifier: "ItemCell") as? ItemCell else {
            return UITableViewCell()
        }
        
        cell.TextCell.text = menu_data.text_data
        cell.ImageView.loadImage(urlString: menu_data.image)
        
        return cell
    }
}


