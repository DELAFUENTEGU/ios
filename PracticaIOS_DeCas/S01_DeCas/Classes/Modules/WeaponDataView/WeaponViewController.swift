//
//  WeaponViewController.swift
//  PracticaIOS_DeCas
//
//  Created by Alejandro Castro on 16/12/17.
//  Copyright © 2017 DeCas. All rights reserved.
//

import UIKit

class WeaponViewController: UIViewController {

    @IBOutlet var tableView: UITableView!
    
    // Weapon dara array
    var weapon_data : [WeaponData] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Title view
        self.title = "Weapons"
        // Configure the weapon data table
        configureTable()
        // Configure the weapon data array
        configureWeaponData()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Configure the weapon data table
    private func configureTable(){
        self.tableView.dataSource = self

        self.tableView.separatorStyle = .singleLine
        self.tableView.rowHeight = 161 //UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 161
        
        let bandCellNib = UINib(nibName: "WeaponCell", bundle: nil)
        
        // Add the cell to the table
        self.tableView.register(bandCellNib, forCellReuseIdentifier: "WeaponCell")
    }
    
    // Read the url from the API and store the values in the weapon data array
    private func configureWeaponData(){
            // Read the url from the API
            API().getWeaponData { wdata in
                // Store the data in the weapon data array
                self.weapon_data = wdata
                // Reload using main thread
                DispatchQueue.main.async(execute: {
                self.tableView.reloadData()
                })
        }
    }
}

// Extension
extension WeaponViewController: UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
}

// Extension
extension WeaponViewController : UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    // Return the number of elements of the weapon data array
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.weapon_data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Get the data of a element in the weapon data array
        let data_info = self.weapon_data[indexPath.row]
        // Configure the cell
        return self.configureWeaponTableViewCell(weapon_data: data_info)
    }
    
    // Configure a cell for the table
    func configureWeaponTableViewCell(weapon_data: WeaponData) -> UITableViewCell {
        
        guard let cell = self.tableView.dequeueReusableCell(withIdentifier: "WeaponCell") as? WeaponCell else {
            return UITableViewCell()
        }
        
        cell.WeaponNameText.text = weapon_data.name
        cell.DamageText.text = weapon_data.damage
        cell.AmmoText.text = weapon_data.ammo
        cell.FireRateText.text = weapon_data.firerate
        cell.ImageView.loadImage(urlString: weapon_data.image)
        return cell
    }
}
