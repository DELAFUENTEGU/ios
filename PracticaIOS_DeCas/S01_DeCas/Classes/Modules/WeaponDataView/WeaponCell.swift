//
//  WeaponCell.swift
//  PracticaIOS_DeCas
//
//  Created by Alejandro Castro on 16/12/17.
//  Copyright © 2017 DeCas. All rights reserved.
//

import UIKit

class WeaponCell: UITableViewCell {

    @IBOutlet var WeaponNameText: UILabel!
    @IBOutlet var ImageView: UIImageView!
    @IBOutlet var FireRateText: UILabel!
    @IBOutlet var RangeText: UILabel!
    @IBOutlet var AmmoText: UILabel!
    @IBOutlet var DamageText: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
