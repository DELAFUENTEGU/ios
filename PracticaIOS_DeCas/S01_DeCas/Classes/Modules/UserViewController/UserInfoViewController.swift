//
//  UserInfoViewController.swift
//  PracticaIOS_DeCas
//
//  Created by Alejandro Castro on 17/12/17.
//  Copyright © 2017 DeCas. All rights reserved.
//

import UIKit
import KFSwiftImageLoader

class UserInfoViewController: UIViewController {

    @IBOutlet var UserImage: UIImageView!
    @IBOutlet var UserName: UILabel!
    @IBOutlet var UserWins: UILabel!
    @IBOutlet var UserLosts: UILabel!
    @IBOutlet var UserKills: UILabel!
    @IBOutlet var UserFavourite: UILabel!
    
    
    override func viewDidLoad() {
        
        //print(Global.instance.user_id)
        
        super.viewDidLoad()
        
        loadInfo()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func loadInfo(){
        
        //self.UserImage.loadImage(urlString: Global.instance.users[Global.instance.user_id].image)
        
        self.UserImage.loadImage(urlString: Global.instance.userdefaults.string(forKey: "u_image")!)
        
        //self.UserName.text = Global.instance.users[Global.instance.user_id].name
        
        self.UserName.text = Global.instance.userdefaults.string(forKey: "u_name")
        
        //self.UserWins.text = Global.instance.users[Global.instance.user_id].games_wins
        self.UserWins.text = Global.instance.userdefaults.string(forKey: "u_wins")
        
        //self.UserLosts.text = Global.instance.users[Global.instance.user_id].games_losts
        
        self.UserLosts.text = Global.instance.userdefaults.string(forKey: "u_lost")
        
        
        //self.UserKills.text = Global.instance.users[Global.instance.user_id].total_kills
        self.UserKills.text = Global.instance.userdefaults.string(forKey: "u_kills")
        
        //self.UserFavourite.text = Global.instance.users[Global.instance.user_id].fav_weapon
        self.UserFavourite.text = Global.instance.userdefaults.string(forKey: "u_fav")
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

