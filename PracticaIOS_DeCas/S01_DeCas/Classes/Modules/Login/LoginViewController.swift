//
//  LoginViewController.swift
//  PracticaIOS_DeCas
//
//  Created by pvesat on 5/12/17.
//  Copyright © 2017 DeCas. All rights reserved.
//

import UIKit

// Singleton for the app
class Global {
    static let instance = Global()
    // UserDefaults constant
    let userdefaults = UserDefaults.standard
}

class LoginViewController: UIViewController {

    @IBOutlet weak var TF_User: UITextField!
    @IBOutlet weak var TF_Passsword: UITextField!
    @IBOutlet weak var BTN_Login: UIButton!
    
    // Array of users
    var users: [GameUsers] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Setup the UI
        self.setupUI()
        // Do any additional setup after loading the view.
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func setupUI() {
        // Title of the View
        self.title = "Login"
        // Configure the users
        self.configureUsers()
    }
    
    //Get the users from the API and store it in the users array
    private func configureUsers(){
        // Get the users from the API
        API().getUsers { user in
            // Store the users
            self.users = user
            // Reload using main thread
        }
        
    }
    
    // Button pressed
    @IBAction func pressed(_ sender: UIButton) {
      CheckData()

    }
    
    // Verifies if the login data is correct or not
    func CheckData(){
        // Flag to check if the user logs in
        var log_failed = false
        // Check the data saved in the userdefaults
        if(TF_User.text == Global.instance.userdefaults.string(forKey: "u_name") &&  TF_Passsword.text == Global.instance.userdefaults.string(forKey: "u_password")){
           
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                // If the app can not read the json
                if (users.count == 0){
                    // Prepare the user info view
                   let controller = storyboard.instantiateViewController(withIdentifier: "UserInfoViewController")
                    self.navigationController?.pushViewController(controller, animated: true)
                }
                else {
                    // Prepare the menu view
                   let controller = storyboard.instantiateViewController(withIdentifier: "Menu")
                    self.navigationController?.pushViewController(controller, animated: true)
                }
        
        }
        else{
        // If the app can get the data fron the json
        for i in users.count {
            if(TF_User.text == users[i].name && TF_Passsword.text == users[i].password){
                
                // Store the user info in userdefaults
                Global.instance.userdefaults.object(forKey: "user")
                
                Global.instance.userdefaults.set(users[i].name, forKey: "u_name")
                Global.instance.userdefaults.set(users[i].password, forKey: "u_password")
                
                Global.instance.userdefaults.set(users[i].image, forKey: "u_image")
                Global.instance.userdefaults.set(users[i].games_wins, forKey: "u_wins")
                Global.instance.userdefaults.set(users[i].games_losts, forKey: "u_lost")
                Global.instance.userdefaults.set(users[i].total_kills, forKey: "u_kills")
                Global.instance.userdefaults.set(users[i].fav_weapon, forKey: "u_fav")
    
                log_failed = false
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                // Load the menu view
                let controller = storyboard.instantiateViewController(withIdentifier: "Menu")
              self.navigationController?.pushViewController(controller, animated: true)
                break
            }
            else{
                log_failed = true
            }
        }
        }
            // When log fails, show the alert
            if(log_failed){
                let alert = UIAlertView(title: "Error", message: "Wrong username or password!", delegate: nil, cancelButtonTitle: "Ok")
                alert.show()
                log_failed = false
            }
    }
    
    
}//end of the class, now is time to add extensions.

//some stuff we learned in StackOverflow to use an integer for a loop.
extension Int: Sequence {
    public func makeIterator() -> CountableRange<Int>.Iterator {
        return (0..<self).makeIterator()
    }
}


