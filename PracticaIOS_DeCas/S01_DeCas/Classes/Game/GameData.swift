//
//  GameData.swift
//  PracticaIOS_DeCas
//
//  Created by pvesat on 5/12/17.
//  Copyright © 2017 DeCas. All rights reserved.
//

import Foundation

//Strutc to get the user information.
struct GameUsers {

    var name: String
    var password: String
    var image: String
    var games_wins: String
    var games_losts: String
    var total_kills: String
    var fav_weapon: String
    

    init(name: String, password: String, image: String, games_wins: String, games_losts: String, total_kills:String, fav_weapon: String) {
        
        self.name = name
        self.password = password
        self.image = image
        self.games_wins = games_wins
        self.games_losts = games_losts
        self.total_kills = total_kills
        self.fav_weapon = fav_weapon
    }

}




