//
//  WeaponData.swift
//  PracticaIOS_DeCas
//
//  Created by Alejandro Castro on 16/12/17.
//  Copyright © 2017 DeCas. All rights reserved.
//

import Foundation

//Struct to charge the game weapons information.
struct WeaponData {
    
    var name: String
    var ammo: String
    var damage: String
    var firerate: String
    var image: String
    
    init(name: String, ammo: String, damage: String, firerate: String, image:String) {
        self.name = name
        self.ammo = ammo
        self.damage = damage
        self.firerate = firerate
        self.image = image
        
    }
    
}
