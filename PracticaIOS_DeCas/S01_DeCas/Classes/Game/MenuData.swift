//
//  MenuData.swift
//  PracticaIOS_DeCas
//
//  Created by Alejandro Castro on 7/12/17.
//  Copyright © 2017 DeCas. All rights reserved.
//

import Foundation

//Struct to load the second view
struct MenuData {
    
    var text_data: String
    var image: String
    
    init(text_data: String, image: String) {
        self.text_data = text_data
        self.image = image
    }
    
}
