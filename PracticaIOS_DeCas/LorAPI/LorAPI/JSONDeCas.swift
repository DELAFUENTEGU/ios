//
//  JSONDeCas.swift
//  LorAPI
//
//  Created by Alejandro Castro on 17/12/17.
//  Copyright © 2017 Alejandro Castro. All rights reserved.
//

import UIKit

public class JSONDeCas: NSObject {
    public func userURL()->String{
     return "https://www.jasonbase.com/things/y8Bm.json"
    }
    public func weaponURL()->String{
        return "https://www.jasonbase.com/things/Jokb.json"
    }
    public func menuURL()->String{
        return "https://www.jasonbase.com/things/84oD.json"
    }
    public func getDictionary(data: Data)->NSDictionary{
        let jsonObj = try? JSONSerialization.jsonObject(with: data, options: .allowFragments) as! NSDictionary
        return jsonObj!
    }
}
